<?php

namespace App\Controller;

use App\Repository\TodoRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use App\Entity\Todo;
use App\Form\TodoType;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;


class TodoController extends AbstractController
{

    /**
     * @Route("/todo", name="todo")
     */
    public function index(TodoRepository $repository)
    {
        return $this->render('todo/index.html.twig', [
            'controller_name' => 'TodoController',
            "todos" => $repository->findAll()
        ]);
    }

    /**
     * @Route("/todo/{id}/done", name="todo_done", methods={"GET"})
     */
    public function done(Todo $todo)
    {
        // $todo = $repository->find($id);
        // if(!$todo)
        // {
        //     throw new NotFoundHttpException();
        // }
        $todo->setIsDone(true);
        $todo->setDoneAt(new \DateTime());
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($todo);
        $entityManager->flush();
        // redirect todo index
        return $this->redirectToRoute("todo");
    }

    /**
     * @Route("/todo/{id}/delete", name="todo_delete", methods={"GET"})
     */
    public function delete(string $id, TodoRepository $repository)
    {

    }

    /**
     * @Route("/todo/form/{id}", defaults={"id"=null}, name="todo_form", methods={"POST", "GET"})
     */
    public function form(?string $id=null, Request $request, TodoRepository $repository)
    {
        // pengecekan apakah id tidak null
        if($id){
            // mencari data berdasarkan id untuk edit data
            $todo = $repository->find($id);
        } else {
            // instansiasi Entity Todo
            $todo = new Todo();
        }

        if(!$todo){
            throw new NotFoundHttpException();
        }
        $form = $this->createForm(TodoType::class, $todo);
        $form->handleRequest($request);
        // mengecek apakah tombol submit ditekan dan juga melakukan validasi
        if($form->isSubmitted() && $form->isValid())
        {
            // menangkap data yg diinput dari form
            $todo = $form->getData();
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($todo);
            $entityManager->flush();
            return $this->redirectToRoute("todo");
        }
        return $this->render("todo/create.html.twig", ["form"=>$form->createView()]);
    }

}
