<?php

namespace App\DataFixtures;

use App\Entity\Todo;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $todo = new Todo();
        $todo->setDescription("Belajar Menginstall Framework Symfony");
        $todo->setIsDone(true);
        $todo->setDoneAt(new \DateTime());
        $manager->persist($todo);

        $todo = new Todo();
        $todo->setDescription("Belajar Membuat Todo Framework Symfony");
        $manager->persist($todo);

        $todo = new Todo();
        $todo->setDescription("Belajar Lebih Dalam Tentang Framework Symfony");
        $manager->persist($todo);

        $todo = new Todo();
        $todo->setDescription("Menjadi Master Framework Symfony");
        $manager->persist($todo);

        $manager->flush();
    }
}
